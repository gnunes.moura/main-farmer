const router = require('express').Router();

router.get('/healthcheck', (req, res) => {
  const now = new Date();
  res.status(200).send({ nowUTC: now.toUTCString(), now: now.toString() });
});

module.exports = router;
