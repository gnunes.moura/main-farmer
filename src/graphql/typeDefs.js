const { gql } = require('apollo-server-express');

module.exports = gql`
  type Query {
    temperatureToday: [TemperatureRead]
    relativeHumidityToday: [RelativeHumidityRead]
  }

  type Mutation {
    postTemperatureRead(read: TemperatureReadInput!): TemperatureRead
    postRelativeHumidityRead(read: RelativeHumidityReadInput!): RelativeHumidityRead
  }

  type StateCollector {
    id: ID!
  }

  type TemperatureRead {
    id: ID
    celsius: Float
    timestamp: Float
    collector: StateCollector!
  }

  type RelativeHumidityRead {
    id: ID
    percentage: Float
    timestamp: Float
    collector: StateCollector!
  }

  input TemperatureReadInput {
    celsius: Float!
    timestamp: Float!
    collector: ID!
  }

  input RelativeHumidityReadInput {
    percentage: Float!
    timestamp: Float!
    collector: ID!
  }
`;
