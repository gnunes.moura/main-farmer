const reads = {
  temperatureReads: [
  ],
  relativeHumidityReads: [
  ],
};

module.exports = {
  Query: {
    temperatureToday: () => reads.temperatureReads,
    relativeHumidityToday: () => reads.relativeHumidityReads,
  },
  Mutation: {
    postTemperatureRead: (parent, args) => {
      const { celsius, timestamp, collector: collectorId } = args.read;
      const temperatureRead = {
        id: '1',
        celsius,
        timestamp,
        collector: {
          id: collectorId,
        },
      };
      reads.temperatureReads.push(temperatureRead);
      return temperatureRead;
    },
    postRelativeHumidityRead: (parent, args) => {
      const { percentage, timestamp, collector: collectorId } = args.read;
      const relativeHumidityRead = {
        id: '1',
        percentage,
        timestamp,
        collector: {
          id: collectorId,
        },
      };
      reads.relativeHumidityReads.push(relativeHumidityRead);
      return relativeHumidityRead;
    },
  },
};
