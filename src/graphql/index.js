const { ApolloServer } = require('apollo-server-express');
const { ApolloServerPluginDrainHttpServer } = require('apollo-server-core');
const http = require('http');
const nconf = require('nconf');
const resolvers = require('./resolvers');
const typeDefs = require('./typeDefs');

const path = '/graphql';
const isDevelopment = nconf.get('NODE_ENV') === 'development';

module.exports = async (app) => {
  const httpServer = http.createServer(app);

  const server = new ApolloServer({
    typeDefs,
    resolvers,
    playground: isDevelopment,
    introspection: isDevelopment,
    plugins: [ApolloServerPluginDrainHttpServer({ httpServer })],
  });

  await server.start();
  server.applyMiddleware({ app, path });
};
