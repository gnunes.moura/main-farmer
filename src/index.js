/**
 * Main Farmer.
 * @module main-farmer
 * @typicalname main-farmer
 * @example
 * ```sh
 * docker run -d -p 4001:4001 registry.gitlab.com/gnunes.moura/farmer-crow:latest
 * curl --request POST \
 *   --header 'content-type: application/json' \
 *   --url http://localhost:4001/graphql \
 *   --data '{"query":"query ExampleQuery { books { author title   } }"}'
 * ```
 */

const express = require('express');
const graphqlAPI = require('./graphql');
const routes = require('./rest-routes');

const app = express();

graphqlAPI(app);

app.use(express.json());
app.use('/', routes);

module.exports = app;
