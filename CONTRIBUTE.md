--- 
type: reference, dev 
group: Development 
info: To determine the technical writers associated with this page, see the last
merge that affected this file.
---
# Contribute
Thank you for your interest in contributing to the project. This guide details 
how to contribute in a way that is easy for everyone.

Definition of the [Contribution flow](#contribution-flow).

Looking for something to work on? See the 
[How to contribute](#how-to-contribute) section for more information.

This document is inspired on the
[development contributing guideline of gitlab](1).

## Security vulnerability disclosure
Report suspected security vulnerabilities in private to a Maintainer. If not 
replied by the Maintainer open an issue WITHOUT informations about the security 
vulnerabilities and we will contact you.

[comment]: <> (TODO create private process of security warning like "send a email with the subject "Suspected Security Vulnerabilities" to a suport@project.com)

WARNING: Do **NOT** create publicly viewable issues for suspected security vulnerabilities.

## Code of conduct
If the rules are set, everyone can play.

### The Golden Law

“Don't do unto others what you don't want done unto you.”

In the interest of fostering an open and welcoming environment, we as 
Contributors and Maintainers pledge to making participation in our project and 
our community a harassment-free experience for everyone by following the Golden
Law.

### Our Stanards
Examples of behavior that contributes to creating a positive environment 
include:

1. Using welcoming and inclusive language.
1. Being respectful of differing viewpoints and experiences.
1. Gracefully accepting constructive criticism.
1. Focusing on what is best for the community.
1. Showing empathy towards other community members.

Examples of unacceptable behavior by participants include:

1. The use of sexualized language or imagery and unwelcome sexual attention or 
    advances.
1. Trolling, insulting/derogatory comments, and personal or political attacks.
1. Public or private harassment.
1. Publishing others' private information, such as a physical or electronic
    address, without explicit permission.
1. Other conduct which could reasonably be considered inappropriate in a 
    professional setting.

### Our Responsibilities

Project Maintainers are responsible for clarifying the standards of acceptable 
behavior and are expected to take appropriate and fair corrective action in 
response to any instances of unacceptable behavior.

Project Maintainers have the right and responsibility to remove, edit, or reject
comments, commits, code, wiki edits, issues, and other Contributions that are 
not aligned to this Code of Conduct, or to ban temporarily or permanently any
Contributor for other behaviors that they deem inappropriate, threatening,
offensive, or harmful.

## Closing policy for issues and merge requests
As every project has a finite amount of resources, this project is no exception.
Out of respect for our volunteers, issues and merge requests not in line with 
the guidelines listed in this document may be closed without notice. 

Issues and merge requests should be in English and contain appropriate language 
for audiences of all ages. If a Contributor is no longer actively working on a
submitted merge request, we can:

- Decide that the merge request will be continued by others. 
- Close the merge request.

We make this decision based on how important the change is for our product 
vision.

## How to contribute
Informações que ajudam novos membros

If you would like to contribute to this project: 

- Issues with the `~Accepting merge requests` label are a great place to start. 
- Optimizing our tests is another great opportunity to contribute.
- Consult the [Contribution Flow](#contribution-flow) section to learn the 
    process.

[comment]: <> (Implement a performace profiler to identify slowest tests. These tests are good candidates for improving.)
[comment]: <> (Identify and document best practices)

### Development Kit
To be defined.
[comment]: <> (Create a issue and relate to this to be defined)

### Contribution flow

1. [Create a fork](2) of the project.
1. Make your changes in your fork. 
1. When you're ready, [create a new merge request](3).
1. In the merge request's description: 
    - Ensure you provide complete and accurate information. 
    - Review the provided checklist. 
1. Assign the merge request (if possible) to, or `@mention`, one of the 
    Maintainers of the project, and explain that you are ready for review. 
    
When you submit code to the project, we really want it to get merged! However, 
we always review submissions carefully, and this takes time. Code submissions
will usually be reviewed by two Contributors before being merged:

- A reviewer, any Contributor with developer access or higher.
- A Maintainer.

Keep the following in mind when submitting merge requests:

- When reviewers are reading through a merge request they may request guidance
    from other reviewers. 
- If the code quality is found to not meet the project standards, the merge 
    request reviewer will provide guidance and refer the author to our:
    - [Code style guides](#style-guide).
- Sometimes style guides will be followed but the code will lack structural
    integrity, or the reviewer will have reservations about the code's overall
    quality. When there is a reservation, the reviewer will inform the author
    and provide some guidance.
- The Maintainer may require approvals from certain reviewers before merging a
    merge request.
- After review, the author may be asked to update the merge request. Once the
    merge request has been updated and reassigned to the reviewer, they will
    review the code again. This process may repeat any number of times before
    merge, to help make the contribution the best it can be.

Sometimes a Maintainer may choose to close a merge request. They will fully
disclose why it will not be merged, as well as some guidance. The maintainers
will be open to discussion about how to change the code so it can be approved
and merged in the future and will do its best to review community contributions
as quickly as possible.

When submitting code to the project, you may feel that your contribution
requires the aid of an external library. If your code includes an external
library, please provide a link to the library, as well as reasons for
including it. 

#### Breaking changes
A "breaking change" is any change that requires users to make a corresponding
change to their code, settings, or workflow. "Users" might be humans, API
clients, or even code classes that "use" another class. Examples of breaking
changes include:

- Removing a user-facing feature without a replacement/workaround.
- Changing the definition of an existing API (by re-naming query parameters,
    changing routes, etc.).
- Removing a public method from a code class. A breaking change can be
    considered "major" if it affects many users, or represents a significant
    change in behavior.

### Issues workflow

As this CONTRIBUTE file, the issues workflow will be inspired on the Gitlab
project issue workflow. If needed we will create our issue workflow.

This [documentation](5) outlines the current issue workflow:

- Issue tracker guidelines
- Issue triaging
- Labels
- Feature proposals
- Issue weight
- Regression issues
- Technical debt
- Technical debt in follow-up issues

### Merge requests workflow

We welcome merge requests from everyone, with fixes and improvements to code,
tests, and documentation The issues that are specifically suitable for
contributions are listed with the `Accepting merge requests` label, but you are
free to contribute to any issue you want.

If you want to add a new feature that is not labeled, it is best to first create
an issue (if there isn't one already) and leave a comment asking for it to be
marked as `Accepting Merge Requests`.

#### Merge request guidelines 

If you find an issue, please submit a merge request with a fix or improvement,
if you can, and include tests. If you don't know how to fix the issue but can
write a test that exposes the issue, we will accept that as well. In general,
bug fixes that include a regression test are merged quickly, while new features
without proper tests might be slower to receive feedback. The workflow to make a
merge request is as follows:

1. [Fork](6) the project into your personal namespace (or group) on GitLab.com.
1. Create a feature branch in your fork (don't work off your 
    [default branch](7)).
1. Write tests and code.
1. [Ensure a changelog is created](8).
1. Write documentation for every function, make sure to follow the [JSDoc](9)
    documentation pattern.
1. Follow the [commit messages guidelines](#commit-messages-guidelines).
1. If you have multiple commits, combine them into a few logically organized
    commits by [squashing them](10), but do not change the commit history if
    you're working on shared branches though.
1. Push the commit(s) to your working branch in your fork.
1. Submit a merge request (MR) to the `main` branch in the main GitLab project.
1. Your merge request needs at least 1 approval, but depending on your changes
    you might need additional approvals.
1. You don't have to select any specific approvers, but you can if you really
    want specific people to approve your merge request.
1. The MR title should describe the change you want to make.
1. The MR description should give a reason for your change.
1. Use the syntax `Solves #XXX`, `Closes #XXX`, or `Refs #XXX` to mention the
    issue(s) your merge request addresses. Referenced issues do not close
    automatically. You must close them manually once the merge request is merged.
1. Include any steps or setup required to ensure reviewers can view the changes
    you've made (e.g. include any information about feature flags).  

If you would like quick feedback on your merge request feel free to mention some
Maintainer. When having your code reviewed and when reviewing merge requests,
please keep [The Golden Law](#the-golden-law) in mind.

#### Commit messages guidelines

Commit messages should follow the guidelines below, for reasons explained by
Chris Beams in [How to Write a Git Commit Message](11):

- The commit subject and body must be separated by a blank line.
- The commit subject must start with a capital letter.
- The commit subject must not be longer than 72 characters.
- The commit subject must not end with a period.
- The commit body must not contain more than 72 characters per line.
- The commit subject or body must not contain Emojis.
- Commits that change 30 or more lines across at least 3 files should describe
    these changes in the commit body.
- Use issues and merge requests' full URLs instead of short references, as they
    are displayed as plain text outside of GitLab.
- The merge request should not contain more than 10 commit messages.
- The commit subject should contain at least 3 words.

**Why these standards matter**

1. Consistent commit messages that follow these guidelines make the history more
    readable.
1. Concise standard commit messages helps to identify[breaking changes](#breaking-changes)
    for a deployment or ~"main:broken" quicker when reviewing commits between
    two points in time.

#### Contribution acceptance criteria 

To make sure that your merge request can be approved, please ensure that it
meets the contribution acceptance criteria below:

1. The change is as small as possible.
1. Include proper tests and make all tests pass (unless it contains a test
    exposing a bug in existing code). Every new class should have corresponding
    unit tests, even if the class is exercised at a higher level, such as a
    feature test.
- If a failing CI build seems to be unrelated to your contribution, you can try
    restarting the failing CI job, rebasing from `main` to bring in updates that
    may resolve the failure, or if it has not been fixed yet, ask a developer to
    help you fix the test.
1. The MR initially contains a few logically organized commits.
1. The changes can merge without problems. If not, you should rebase if you're
    the only one working on your feature branch, otherwise merge `main`.
1. Only one specific issue is fixed or one specific feature is implemented. Do
    not combine things; send separate merge requests for each issue or feature.
1. Migrations should do only one thing (e.g., create a table, move data to a new
    table, or remove an old table) to aid retrying on failure.
1. Contains functionality that other users will benefit from.
1. Doesn't add configuration options or settings options since they complicate
    making and testing future changes.
1. Changes do not degrade performance:
    - Avoid repeated polling of endpoints that require a significant amount of overhead.
    - Avoid repeated access of the file system.
1. The merge request meets the [definition of done](#definition-of-done), below.

#### Definition of done

If you contribute to the project please know that changes involve more than just
code. We use the following definition of done. Your contribution is not *done*
until you have made sure it meets all of these requirements.

1. Clear description explaining the relevancy of the contribution.
1. Working and clean code that is commented where needed.
1. Unit, integration, and system tests that all pass on the CI server.
1. Regressions and bugs are covered with tests that reduce the risk of the issue
    happening again.
1. Performance guidelines have been followed. *To be created*
1. Secure coding guidelines have been followed. *To be created*
1. Documented.
1. [Changelog entry added](8), if necessary.
1. Merged by a project maintainer.
1. Black-box tests/end-to-end tests added if required
1. The new feature does not degrade the user experience of the product.

## Style guide
We use [Airbnb's JavaScript Style Guide](12) and its accompanying linter to
manage most of our JavaScript style guidelines.

Any specific rule are listed on the `.eslintrc`.



[1]: https://gitlab.com/gitlab-org/gitlab/-/raw/master/doc/development/contributing/index.md
[2]: https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html
[3]: https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html
[4]: https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/issue_templates/Bug.md
[5]: https://docs.gitlab.com/ee/development/contributing/issue_workflow.html
[6]: https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html
[7]: https://www.google.com/search?q=default+branch+gitlab
[8]: https://docs.gitlab.com/ee/development/changelog.html
[9]: https://jsdoc.app/
[10]: https://git-scm.com/book/en/v2/Git-Tools-Rewriting-History#_squashing
[11]: https://chris.beams.io/posts/git-commit/
[12]: https://github.com/airbnb/javascript
[generate-changelog]: https://docs.gitlab.com/ee/api/repositories.html#generate-changelog-data
